import { Component, Vue } from 'vue-property-decorator'

@Component
export default class PageBase extends Vue {
  isEdit: boolean = true

  mounted(): void {
    window.addEventListener('beforeunload', this.beforeUnloadHandler, true)
    this.$once('hook:beforeDestroy', () => {
      window.removeEventListener('beforeunload', this.beforeUnloadHandler, true)
    })
  }

  beforeUnloadHandler(event: BeforeUnloadEvent) {
    event.preventDefault()
    event.returnValue = ''
  }

  beforeRouteLeave(to: any, from: any, next: any) {
    if (this.isEdit) {
      this.$confirm('当前页面正处于编辑中，确认离开？', '提示', {
        type: 'warning'
      })
        .then(() => {
          next()
        })
        .catch(() => {
          next(false)
        })
    } else {
      next()
    }
  }
}

import Home from '../views/Home.vue'
import Layout from '../views/Layout.vue'

export default [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/page',
    name: 'page',
    component: Layout,
    redirect: { name: 'page-a' },
    children: [
      {
        path: 'a',
        name: 'page-a',
        component: () =>
          import(/* webpackChunkName: "page" */ '../views/PageA.vue')
      },
      {
        path: 'b',
        name: 'page-b',
        component: () =>
          import(/* webpackChunkName: "page" */ '../views/PageB.vue')
      },
      {
        path: 'c',
        name: 'page-c',
        component: () =>
          import(/* webpackChunkName: "page" */ '../views/PageC.vue')
      },
      {
        path: 'x*',
        name: 'page-x',
        props: true,
        component: () =>
          import(/* webpackChunkName: "page" */ '../views/PageX.vue')
      }
    ]
  }
]

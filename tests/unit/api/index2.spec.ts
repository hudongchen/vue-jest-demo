jest.mock('@/api/request', () => ({
  GET: () => Promise.resolve([{ username: 'Xyz' }])
}))
import Api from '@/api' // eslint-disable-line import/first

xdescribe('@/api/index.ts', () => {
  it('getUsers接口，不接收入参', () => {
    return Api.getUsers().then((res: any) => {
      expect(res.length).toBe(1)
      expect(res[0].username).toMatch('Xyz')
    })
  })
})

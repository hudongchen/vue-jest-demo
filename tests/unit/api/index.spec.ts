import Api from '@/api'

describe('@/api/index.ts', () => {
  it('getUsers接口，不接收入参', () => {
    return Api.getUsers().then((res: any) => {
      expect(res.length).toBe(10)
    })
  })

  it('getUsers接口，接收入参', async () => {
    const res: any = await Api.getUsers({ id: 1 })
    expect(res.length).toBe(1)
    expect(res[0].id).toBe(1)
  })
})
